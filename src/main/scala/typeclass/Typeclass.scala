package katas

import simulacrum.{ op, typeclass }

object TypeclassApp extends App {

  final case class CP(re: Int, im: Int)

  @typeclass trait Ordering[T] {
    def compare(x: T, y: T): Int

    @op("<") def lt(x: T, y: T): Boolean = compare(x, y) < 0
    @op(">") def gt(x: T, y: T): Boolean = compare(x, y) > 0
  }

  import Ordering.ops._

  implicit val CPOrdering: Ordering[CP] = new Ordering[CP] {

    def compare(x: CP, y: CP): Int = if ((x.re == y.re) && (x.im == y.im)) 1 else 0

    override def lt(x: CP, y: CP) = x.re < y.re
    override def gt(x: CP, y: CP) = x.re > y.re
  }

  val a = CP(1, 2)
  val b = CP(2, 3)

  // Prints true
  val res0 = a == a
  println(s"Res = $res0")

  // Prints false
  val res1 = a > b
  println(s"Res = $res1")

}
