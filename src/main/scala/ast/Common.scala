package katas

object Common {

  sealed abstract class ASTType

  final case object IntASTType     extends ASTType
  final case object LongASTType    extends ASTType
  final case object BooleanASTType extends ASTType
  //final case object DoubleASTType   extends ASTType
  //final case object StringASTType   extends ASTType
  //final case object AnyASTType      extends ASTType

}
