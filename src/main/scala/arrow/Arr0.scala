package arr

import cats.arrow.Arrow
import cats.implicits._

object Arr0 extends App {

  def combine[F[_, _]: Arrow, A, B, C](fab: F[A, B], fac: F[A, C]): F[A, (B, C)] =
    Arrow[F].lift((a: A) => (a, a)) >>> (fab *** fac)

  val mean: List[Int] => Double =
    combine((_: List[Int]).sum, (_: List[Int]).size) >>> { case (x, y) => x.toDouble / y }

  val variance: List[Int] => Double =
    // Variance is mean of square minus square of mean
    combine(((_: List[Int]).map(x => x * x)) >>> mean, mean) >>> { case (x, y) => x - y * y }

  val meanAndVar: List[Int] => (Double, Double) = combine(mean, variance)

  val res0: (Double, Double) = meanAndVar(List(1, 2, 3, 4))

  println(res0)

}
