package arr

import cats.implicits._

object Arr1 extends App {

  // Compose, endThen
  val f = (_: Int) + 1
  val g = (_: Int) * 100
  val h = (_: Int) * 5

  val res0: Int = (f >>> g >>> h)(2)
  val res1: Int = (f <<< g <<< h)(2)

  println(s"res0 = $res0")
  println(s"res1 = $res1")

  // First, second
  val f_first: ((Int, Int)) => (Int, Int)  = f.first[Int]
  val f_second: ((Int, Int)) => (Int, Int) = f.second[Int]

  val res2: (Int, Int) = f_first((1, 1))
  val res3: (Int, Int) = f_second((1, 1))
  println(s"res2 = $res2")
  println(s"res3 = $res3")

  // Split
  val res4: (((Int, Int), Int), Int) = (f *** g *** h *** h)((((1, 1), 1), 1))
  println(s"res4 = $res4")

}
