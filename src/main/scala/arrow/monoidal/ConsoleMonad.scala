package arrow.monoidal
import cats.{ Monad, StackSafeMonad }

sealed abstract class ConsoleM[A]

object ConsoleM {

  final case class Pure[A](x: A)                                     extends ConsoleM[A]
  final case class Bind[A, B](x: ConsoleM[A], fab: A => ConsoleM[B]) extends ConsoleM[B]
  final case object GetLine                                          extends ConsoleM[String]
  final case class PutLine(s: String)                                extends ConsoleM[Unit]

  implicit val monad: Monad[ConsoleM] = new StackSafeMonad[ConsoleM] {
    def pure[A](x: A): ConsoleM[A]                                       = Pure(x)
    def flatMap[A, B](fa: ConsoleM[A])(f: A => ConsoleM[B]): ConsoleM[B] = Bind(fa, f)
  }

  val getLine: ConsoleM[String] = GetLine

  // def echo2: ConsoleM[Unit] = for (x <- getLine; y <- getLine) yield x + y

  def countGets[A](cm: ConsoleM[A]): Int = cm match {
    case Pure(_)    => 0
    case GetLine    => 1
    case PutLine(_) => 0
    // case Bind(m, f) => countGets(m) + (???:Int) ///+ (() => countGets(f))
    case Bind(m, f) => countGets(m) //+ countGets(f)

  }

}
