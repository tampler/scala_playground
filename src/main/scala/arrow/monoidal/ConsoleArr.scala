package arrow.monoidal

import cats.arrow.Arrow
import cats.implicits._

sealed abstract class ConsoleArr[A, B]

object ConsoleArr {

  final case class Lift[A, B](f: A => B)                                             extends ConsoleArr[A, B]
  final case class AndThen[A, B, C](start: ConsoleArr[A, B], next: ConsoleArr[B, C]) extends ConsoleArr[A, C]
  final case class Split[A, B, C, D](first: ConsoleArr[A, B], second: ConsoleArr[C, D])
      extends ConsoleArr[(A, C), (B, D)]

  final case object GetLine extends ConsoleArr[Unit, String]
  final case object PutLine extends ConsoleArr[String, Unit]

  implicit val arr: Arrow[ConsoleArr] = new Arrow[ConsoleArr] {
    def lift[A, B](f: A => B): ConsoleArr[A, B]                                      = Lift(f)
    def compose[A, B, C](f: ConsoleArr[B, C], g: ConsoleArr[A, B]): ConsoleArr[A, C] = AndThen(g, f)
    def first[A, B, C](fa: ConsoleArr[A, B]): ConsoleArr[(A, C), (B, C)]             = Split(fa, id)

  }

  val getLine: ConsoleArr[Unit, String] = GetLine
  val putLine: ConsoleArr[String, Unit] = PutLine

  def concat: ConsoleArr[(String, String), String] = Lift(Function.tupled(_ + _))

  def echo2: ConsoleArr[Unit, Unit] = (getLine &&& getLine) >>> concat >>> putLine

  def countGets[A, B](carr: ConsoleArr[A, B]): Int = carr match {
    case Lift(_)              => 0
    case AndThen(start, next) => countGets(start) + countGets(next)
    case Split(first, second) => countGets(first) + countGets(second)
    case GetLine              => 1
    case PutLine              => 0
  }
}
