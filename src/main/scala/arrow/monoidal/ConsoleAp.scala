package arrow.monoidal
import cats.{ Applicative }

sealed abstract class ConsoleA[A]

object ConsoleA {

  final case class Pure[A](x: A)                                 extends ConsoleA[A]
  final case class Ap[A, B](f: ConsoleA[A => B], x: ConsoleA[A]) extends ConsoleA[B]

  final case object GetLine           extends ConsoleA[String]
  final case class PutLine(s: String) extends ConsoleA[Unit]

  implicit val applicative: Applicative[ConsoleA] = new Applicative[ConsoleA] {
    def pure[A](x: A): ConsoleA[A]                                   = Pure(x)
    def ap[A, B](ff: ConsoleA[A => B])(fa: ConsoleA[A]): ConsoleA[B] = Ap(ff, fa)
  }

  val getLine: ConsoleA[String] = GetLine

  def echo2: ConsoleA[Unit] = ???

  def countGets[A](cm: ConsoleA[A]): Int = cm match {
    case Pure(_)    => 0
    case GetLine    => 1
    case PutLine(_) => 0
    case Ap(f, x)   => countGets(f) + countGets(x)
  }

}
