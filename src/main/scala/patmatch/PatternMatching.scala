// This shows how to match patterns on coproducts

package katas
import Common._

object PatternMatchingApp extends App {

  def check(a: ASTType): String = a match {
    case _: IntASTType.type => "Int type"
    case _                  => "Unknown type"
  }

  val res0: String = check(IntASTType)
  val res1: String = check(LongASTType)

  println(s"res0 = $res0")
  println(s"res1 = $res1")

}
