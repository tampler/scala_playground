package graph

import scalax.collection.Graph // or scalax.collection.mutable.Graph
import scalax.collection.GraphPredef._
import scalax.collection.GraphEdge
// import scalax.collection.GraphEdge._

object App0 extends App {

  val g0: Graph[Int, GraphEdge.UnDiEdge] = Graph(3 ~ 1, 5)
  println(g0.mkString(","))

  val g1: Graph[Int, GraphEdge.DiEdge] = Graph(1 ~> 2)
  println(g1)

  val n1: g1.NodeT = g1.nodes.head
  println(n1)

  val e1: g1.EdgeT = g1.edges.head
  println(e1)

}
