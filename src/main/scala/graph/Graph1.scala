package graph

import scalax.collection.Graph
import scalax.collection.GraphEdge._
// import graph.{ MyEdge, MyNode }
import scalax.collection.GraphEdge

case class MyNode(id: Int, op: Function1[Int, Int])

case class MyEdge(
  val nodeFrom: MyNode,
  val nodeTo: MyNode
) extends DiEdge(NodeProduct(nodeFrom, nodeTo))

object App1 extends App {

  val f = (_: Int) + 1
  val g = (_: Int) * 2
  val h = (_: Int) - 3

  val n1: MyNode = MyNode(1, f)
  val n2: MyNode = MyNode(2, g)
  val n3: MyNode = MyNode(3, h)

  val e1: MyEdge = MyEdge(n1, n2)
  val e2: MyEdge = MyEdge(n2, n3)

  val nset: Set[MyNode]                   = Set(n1, n2, n3)
  val eset: Set[MyEdge]                   = Set(e1, e2)
  val g1: Graph[MyNode, GraphEdge.DiEdge] = Graph.from(nset, eset)

  println(g1.isAcyclic)
  println(g1.isComplete)

  val res: Int = g1.nodes.foldLeft(0) { case (cum, n) => cum + n.op(1) }
  println(res)

}
