package ZIOIntro

import zio._
import zio.DefaultRuntime

object Env extends App {

  case class Config(server: String, port: Int)

  // Exec Effects
  val runtime: DefaultRuntime = new DefaultRuntime {}

  def run(args: List[String]): ZIO[Environment, Nothing, Int] =
    test.provide(Config("111", 222)).fold(_ => 1, _ => 0)

  val test: ZIO[Config, Nothing, String] =
    for {
      server <- ZIO.access[Config](_.server)
      port   <- ZIO.access[Config](_.port)
    } yield s"Server: $server, port: $port"
}
