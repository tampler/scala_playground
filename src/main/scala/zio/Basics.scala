package ZIOIntro

import zio._
import zio.console._
import zio.{ DefaultRuntime, ZIO }

object Basics extends App {

  // Exec Effects
  val runtime: DefaultRuntime = new DefaultRuntime {}

  def run(args: List[String]): ZIO[Any, Nothing, Int] =
    Top.fold(_ => 1, _ => 0)

  val Top: ZIO[Any, Nothing, Unit] = for {
    s1 <- ZIO.succeed(42)
    _  = runtime.unsafeRun(putStrLn(s"res = $s1"))
  } yield ()

}
