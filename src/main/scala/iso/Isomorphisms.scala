package katas

import cats._
import cats.implicits._
// import cats.arrow.Arrow
import org.scalacheck.{ Arbitrary, Prop }

object Isomorphisms {

  // Define Iso operator
  type <=>[A, B]    = IsoSet[A, B]
  type IsoSet[A, B] = Isomorphism[Function1, A, B]

  trait Isomorphism[F[_, _], A, B] {
    def to: F[A, B]
    def from: F[B, A]
  }

  // Test Arrow Composition
  def arrowCompose[A, B](f: A => B, g: A => B)(implicit ev1: Eq[B], ev2: Arbitrary[A]): Prop =
    Prop.forAll { a: A =>
      f(a) === g(a)
    }

}
