package katas
import cats.kernel.Eq

import Common._
import Isomorphisms._

import org.scalacheck.{ Arbitrary, Gen }
import cats._
import katas.Isomorphisms.<=>

object ASTApp extends App {

  // isomorphic methods
  val isoASTBoolean: Common.ASTType <=> Boolean = new (ASTType <=> Boolean) {

    val to: ASTType => Boolean = {
      case v: BooleanASTType.type if (v == true) => true
      case _                                     => false
    }

    val from: Boolean => ASTType = {
      case v: Boolean => BooleanASTType
    }
  }

  // Test Isomorphism
  implicit val ASTEqual: Eq[Common.ASTType] = Eq.fromUniversalEquals[ASTType]
  implicit val BooleanEqual: Eq[Boolean]    = Eq.fromUniversalEquals[Boolean]

  implicit val arbAST: Arbitrary[ASTType] = Arbitrary {
    Gen.oneOf(BooleanASTType, IntASTType)
  }

  implicit val arbBoolean: Arbitrary[Boolean] = Arbitrary {
    Gen.oneOf(true, false)
  }

  // Actual checks
  //arrowCompose (isoASTBoolean.from  compose isoASTBoolean.to  , identity[ASTType] _).check
  //arrowCompose (isoASTBoolean.to    compose isoASTBoolean.from, identity[Boolean] _).check

  val res0: Common.ASTType = isoASTBoolean.from(false)
  println(s"res0 = $res0")

  val res1: Boolean = isoASTBoolean.to(BooleanASTType)
  println(s"res1 = $res1")

}
