package katas
import cats.kernel.Eq

import Isomorphisms._

import org.scalacheck.{ Arbitrary, Gen }
import cats._, cats.implicits._
import katas.Isomorphisms.<=>
import org.scalacheck.Prop

object IsoApp extends App {

  // isomorphic coproducts
  sealed abstract class Family
  final case object Mother extends Family
  final case object Father extends Family
  final case object Child  extends Family

  sealed abstract class Relic
  final case object Feather extends Relic
  final case object Stone   extends Relic
  final case object Flower  extends Relic

  // isomorphic methods
  val isoFamilyRelic: IsoApp.Family <=> IsoApp.Relic = new (Family <=> Relic) {

    val to: Family => Relic = {
      case Mother => Feather
      case Father => Stone
      case Child  => Flower
    }

    val from: Relic => Family = {
      case Feather => Mother
      case Stone   => Father
      case Flower  => Child
    }
  }

  // Test arrow composition
  val p1: Prop = arrowCompose((_: Int) + 2, 1 + (_: Int))
  val p2: Prop = arrowCompose((_: Int) + 2, 2 + (_: Int))

  p1.check
  p2.check

  // Test Isomorphism
  implicit val familyEqual: Eq[IsoApp.Family] = Eq.fromUniversalEquals[Family]
  implicit val relicEqual: Eq[IsoApp.Relic]   = Eq.fromUniversalEquals[Relic]

  implicit val arbFamily: Arbitrary[Family] = Arbitrary {
    Gen.oneOf(Mother, Father, Child)
  }

  implicit val arbRelic: Arbitrary[Relic] = Arbitrary {
    Gen.oneOf(Feather, Stone, Flower)
  }

  // Actual checks
  val f: IsoApp.Relic => IsoApp.Family = isoFamilyRelic.from
  val g: IsoApp.Family => IsoApp.Relic = isoFamilyRelic.to

  val FamilyId: IsoApp.Family => IsoApp.Family = identity[Family] _
  val RelicId: IsoApp.Relic => IsoApp.Relic    = identity[Relic] _

  arrowCompose(f.compose(g), FamilyId).check
  arrowCompose(g.compose(f), RelicId).check

  // Manual check

  // Prints Stone
  val res0: IsoApp.Relic = g(Father)
  println(s"res0 = $res0")

  // Prints Mother
  val res1: IsoApp.Family = f(Feather)
  println(s"res1 = $res1")

  // Prints Child
  val res2: IsoApp.Family = f.compose(g)(Child)
  println(s"res2 = $res2")

  // Prints Flower
  val res3: IsoApp.Relic = g.compose(f)(Flower)
  println(s"res3 = $res3")

}
