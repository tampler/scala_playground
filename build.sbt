val ZioVersion           = "1.0.0-RC12"
val Specs2Version        = "4.7.0"
val CatsVersion          = "2.0.0-RC2"
val ScalaCheckVersion    = "1.14.0"
val SimulacrumVersion    = "0.19.0"
val kindProjectorVersion = "0.10.3"
val graphVersion         = "1.13.0"

resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  Resolver.sonatypeRepo("snapshots")
)

lazy val root = (project in file("."))
  .settings(
    organization := "Neurodyne",
    name := "katas",
    version := "0.0.1",
    scalaVersion := "2.12.9",
    maxErrors := 3,
    libraryDependencies ++= Seq(
      "dev.zio"              %% "zio"        % ZioVersion,
      "org.typelevel"        %% "cats-core"  % CatsVersion,
      "org.scalacheck"       %% "scalacheck" % ScalaCheckVersion,
      "com.github.mpilquist" %% "simulacrum" % SimulacrumVersion,
      "org.scala-graph"      %% "graph-core" % graphVersion
    )
  )

// Refine scalac params from tpolecat
scalacOptions --= Seq(
  "-Xfatal-warnings"
)

//scalacOptions ++= Seq(
//  "-language:postfixOps"
//)

addCompilerPlugin(scalafixSemanticdb)

addCompilerPlugin("org.typelevel" %% "kind-projector" % kindProjectorVersion)
addCompilerPlugin(("org.scalamacros" % "paradise" % "2.1.1").cross(CrossVersion.full))

// Aliases
addCommandAlias("com", "all compile test:compile it:compile")
addCommandAlias("lint", "; compile:scalafix --check ; test:scalafix --check")
addCommandAlias("fix", "all compile:scalafix test:scalafix")
addCommandAlias("fmt", "all scalafmtSbt scalafmtAll")
addCommandAlias("chk", "all scalafmtSbtCheck scalafmtCheckAll")
addCommandAlias("cov", "; clean; coverage; test; coverageReport")
